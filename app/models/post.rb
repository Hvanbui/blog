class Post < ActiveRecord::Base
  has_many :comments
  validates_presense_of :title
  validates_presense_of :body
end
